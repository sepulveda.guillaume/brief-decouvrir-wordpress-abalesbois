<?php
/**
 * La configuration de base de votre installation WordPress.
 *
 * Ce fichier est utilisé par le script de création de wp-config.php pendant
 * le processus d’installation. Vous n’avez pas à utiliser le site web, vous
 * pouvez simplement renommer ce fichier en « wp-config.php » et remplir les
 * valeurs.
 *
 * Ce fichier contient les réglages de configuration suivants :
 *
 * Réglages MySQL
 * Préfixe de table
 * Clés secrètes
 * Langue utilisée
 * ABSPATH
 *
 * @link https://fr.wordpress.org/support/article/editing-wp-config-php/.
 *
 * @package WordPress
 */

// ** Réglages MySQL - Votre hébergeur doit vous fournir ces informations. ** //
/** Nom de la base de données de WordPress. */
define( 'DB_NAME', 'ABALESBOIS' );

/** Utilisateur de la base de données MySQL. */
define( 'DB_USER', 'root' );

/** Mot de passe de la base de données MySQL. */
define( 'DB_PASSWORD', 'root' );

/** Adresse de l’hébergement MySQL. */
define( 'DB_HOST', 'localhost' );

/** Jeu de caractères à utiliser par la base de données lors de la création des tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/**
 * Type de collation de la base de données.
 * N’y touchez que si vous savez ce que vous faites.
 */
define( 'DB_COLLATE', '' );

/**#@+
 * Clés uniques d’authentification et salage.
 *
 * Remplacez les valeurs par défaut par des phrases uniques !
 * Vous pouvez générer des phrases aléatoires en utilisant
 * {@link https://api.wordpress.org/secret-key/1.1/salt/ le service de clés secrètes de WordPress.org}.
 * Vous pouvez modifier ces phrases à n’importe quel moment, afin d’invalider tous les cookies existants.
 * Cela forcera également tous les utilisateurs à se reconnecter.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'X{Y*Su.op7qD5^t7P)P`AF W]dpYkBJ-1NN&d<>POOipmO_*jsP(*nV[6~vokw5/' );
define( 'SECURE_AUTH_KEY',  'QS[UDL?T%uuC[lNpzn73}())t48GI`/~0-|@>-}t ?S90BaP1[MsXyg]{E%LT6,,' );
define( 'LOGGED_IN_KEY',    'ofgV&Jgh|ObC3[:QV,5Cum~U=%2h7KZq]K-4:KUY;S!j;V^$Ef?pj2|rQNdaUwb|' );
define( 'NONCE_KEY',        ')w_l#F-Tlcv;a:{8 G_qOF|TM*yGeU%tQO@`C*opH<piZ1K|Riuhc^c`w)u$  uC' );
define( 'AUTH_SALT',        'u|Y*Mm2=_B<|2jm.T)r} [ASkkJfqy1qF5!EW<hw6@uKeW[>{/(1V?`y>I<}B]3v' );
define( 'SECURE_AUTH_SALT', '@ee)ZK/ZAnZL&SOeVXGdRY~-vcjw~X*-ioi,S0RUjX:mG7v#%Z`(Nrck;={R/AaZ' );
define( 'LOGGED_IN_SALT',   '!]nb_N^ry9n(P^Sr^mGJpc|&S}}<>-]r<#3?Rb>pg8JFL6{qn~q(D~6:X#N(2V7X' );
define( 'NONCE_SALT',       'Hu^IiMJHRtPbKHpwx$dDyyh7 $=Av4^g_%#Ix0OkA7L@f;78XWQ+>~VVmHVJH=:q' );
/**#@-*/

/**
 * Préfixe de base de données pour les tables de WordPress.
 *
 * Vous pouvez installer plusieurs WordPress sur une seule base de données
 * si vous leur donnez chacune un préfixe unique.
 * N’utilisez que des chiffres, des lettres non-accentuées, et des caractères soulignés !
 */
$table_prefix = 'wp_';

/**
 * Pour les développeurs : le mode déboguage de WordPress.
 *
 * En passant la valeur suivante à "true", vous activez l’affichage des
 * notifications d’erreurs pendant vos essais.
 * Il est fortement recommandé que les développeurs d’extensions et
 * de thèmes se servent de WP_DEBUG dans leur environnement de
 * développement.
 *
 * Pour plus d’information sur les autres constantes qui peuvent être utilisées
 * pour le déboguage, rendez-vous sur le Codex.
 *
 * @link https://fr.wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* C’est tout, ne touchez pas à ce qui suit ! Bonne publication. */

/** Chemin absolu vers le dossier de WordPress. */
if ( ! defined( 'ABSPATH' ) )
  define( 'ABSPATH', dirname( __FILE__ ) . '/' );

/** Réglage des variables de WordPress et de ses fichiers inclus. */
require_once( ABSPATH . 'wp-settings.php' );
