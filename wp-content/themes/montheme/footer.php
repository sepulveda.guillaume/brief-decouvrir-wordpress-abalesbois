    </div>
    <footer>
        <?php
        /* wp_nav_menu([ // Affiche un menu de navigation
            'theme_location' => 'footer', // Emplacement du thème à utiliser => footer
            'container' => false, // Retire le container mis par défaut avec WP
            'menu_class' => 'navbar-nav mr-auto' // Change la classe du <ul> pour correspondre à celle souhaitée par boostrap
        ])*/
        ?>
        <?php dynamic_sidebar('footer-sidebar'); ?>
        <?php wp_footer() ?>
    </footer>

    </body>
    <script>
        AOS.init({
            duration: 1500,
        });
    </script>

    </html>