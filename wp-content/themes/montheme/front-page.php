<?php get_header() ?>
<?php while (have_posts()) : the_post(); ?>
    <?php the_content() ?>
    <!-- Affiche le contenu de la page d'accueil -->
<?php endwhile; ?>

</div>

<!-- WP Query activités -->
<div class="activite bg-light py-2 my-5">
    <div class="container my-5">
        <h2 class="mb-4">ACTIVITÉS</h2>
        <?php
        $args = array(
            'post_type' => 'activite',
            'order' => 'ASC',
        );

        $my_query = new WP_Query($args);

        if ($my_query->have_posts()) : ?>
            <div class="row pt-5" data-aos="fade-up">
                <?php while ($my_query->have_posts()) : $my_query->the_post(); ?>
                    <div class="col d-flex flex-column flex-wrap align-items-center text-center">
                        <span class="icon-services">
                            <a href="<?= get_site_url(null, 'activites') ?>"><?php the_field('logo') ?></a>
                        </span>
                        <span class="title-services">
                            <strong><?php the_title() ?></strong>
                        </span>
                        <span>
                            <?php the_excerpt() ?>
                        </span>
                    </div>
                <?php endwhile; ?>
            </div>
    </div>
</div>

<?php else : endif;

        wp_reset_postdata(); ?>


<!-- SECTION matériel -->
<div class="materiel bg-white py-2 my-5">
    <div class="container mb-5">
        <h2 class="mb-4">MATÉRIELS</h2>
        <?php

        // WP_Query arguments
        $args = array(
            'post_type'              => array('materiel'),
        );

        // The Query
        $query = new WP_Query($args);

        // The Loop
        if ($query->have_posts()) {
            while ($query->have_posts()) {
                $query->the_post(); ?>
                <div class="row mt-5">
                    <div class="col-6 d-flex flex-column flex-wrap align-items-center justify-content-center text-center" data-aos="fade-right">
                        <p><strong><?php the_field('nom'); ?></strong></p>
                        <p><?php the_field('description'); ?></p>
                    </div>
                    <div class="col-6 text-center" data-aos="fade-left">
                        <img src="<?php the_field('image'); ?>" width="200px" height="120px" alt="matériel">
                    </div>
                </div>

        <?php
            }
        } else {
        } ?>
    </div>
</div>

<?php
wp_reset_postdata();
?>


<!-- WP Query engagements -->
<div class="engagements bg-light py-2 my-5">
    <div class="container my-5">
        <h2 class="mb-4">ENGAGEMENTS</h2>
        <?php

        $args = array(
            'post_type' => 'engagement',
            'order' => 'DES',
        );

        $my_query = new WP_Query($args);

        if ($my_query->have_posts()) : ?>
            <div class="row pt-4" data-aos="fade-up">
                <?php while ($my_query->have_posts()) : $my_query->the_post(); ?>
                    <div class="col d-flex flex-column flex-wrap align-items-center text-center">
                        <span class="icon-services">
                            <a href="<?= get_site_url(null, 'engagements') ?>"><?php the_field('Logo') ?></a>
                        </span>
                        <span class="title-services">
                            <strong><?php the_title() ?></strong>
                        </span>
                        <span>
                            <?php the_excerpt() ?>
                        </span>
                    </div>
                <?php endwhile; ?>
            </div>
    </div>
</div>

<?php else : endif;

        wp_reset_postdata(); ?>


<!-- Secteurs d'interventions -->
<div class="intervention bg-white py-2 my-5">
    <div class="container mb-5" id="intervention">
        <h2 class="mb-4">SECTEURS D'INTERVENTION</h2>
        <div class="row pt-4" data-aos="fade-right">
            <div class="col-6">
                <iframe src="https://www.google.com/maps/d/embed?mid=1QIjqJEbTDAh0rlpGXnSd2cTpYtwxarxv" width="100%" height="400px" style="border:0;" allowfullscreen="" loading="lazy"></iframe>
            </div>
            <div class="col-6 d-flex flex-column justify-content-center" data-aos="fade-left">
                <p>La société <strong>ABALESBOIS</strong> intervient pour vos projets sur une grande partie du <strong>Sud de l'Auvergne</strong>.</p>
                <p>Son emprise s'étend notamment sur les <strong>départements</strong> suivants :</p>
                <ul><strong>
                        <li>Haute-loire,</li>
                        <li>Puy de dôme,</li>
                        <li>Ardèche,</li>
                        <li>Lozère.</li>
                    </strong></ul>
            </div>
        </div>
    </div>
</div>


<!-- WP Query partenaires -->
<div class="partenaires bg-partenaires pt-2 mt-5">
    <div class="container py-5" id="partenaires" data-aos="fade-up">
        <h2 class="mb-4">PARTENAIRES</h2>
        <?php
        $args = array(
            'post_type' => 'partenaires',
            'order' => 'ASC',
        );

        $my_query = new WP_Query($args);

        if ($my_query->have_posts()) : ?>
            <div class="container">
                <div class="row">
                    <?php while ($my_query->have_posts()) : $my_query->the_post(); ?>
                        <div class="col d-flex flex-column flex-wrap align-items-center justify-content-center">
                            <span class="icon-partenaires">
                                <?php the_post_thumbnail('partenaires_logo') ?>
                            </span>
                        </div>
                    <?php endwhile; ?>
                </div>
            </div>
    </div>
</div>

<?php else : endif;

        wp_reset_postdata(); ?>


<?php get_footer() ?>