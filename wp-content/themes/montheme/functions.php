<?php

function montheme_supports()
{
    add_theme_support('title-tag'); // Affiche le nom de la page sur l'onglet de navigation
    add_theme_support('custom-logo');
    add_theme_support('post-thumbnails'); // Permet de mettre des images à la une avec WP
    add_theme_support('menus'); // Permet la création d'un menu de naviagtion
    register_nav_menu('header', 'En tête du menu'); // Enregistrement d'un menu d'en tête
    register_nav_menu('footer', 'Pied de page'); // Enregistrement d'un menu de pied de page
    register_sidebar(array(
        'id' => 'footer-sidebar',
        'name' => 'Footer',
        'before_widget'  => '<div class="site__sidebar__widget %2$s">',
        'after_widget'  => '</div>',
        'before_title' => '<p class="site__sidebar__widget__title">',
        'after_title' => '</p>',
    ));
    add_image_size('post-thumbnail', 250, 250, true); // A chaque fois qu'une image est téléchargé, on ajoute dans uploads une image qui a cette dimension => peut être utilisé comme taille par défaut par la suite
    add_image_size('partenaires_logo', 150, 150, true);
}

function montheme_register_assets()
{ // Cette fonction permet d'enregistrer les styles dans le head et les scripts juste avant la fin du body
    wp_register_style('bootstrap', 'https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css', []);
    wp_register_style('AOS', 'https://unpkg.com/aos@2.3.1/dist/aos.css', []);
    wp_register_script('bootstrap', 'https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js', ['popper', 'jquery'], false, true);
    wp_register_script('popper', 'https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js', [], false, true);
    wp_deregister_script('jquery');
    wp_register_script('jquery', 'https://code.jquery.com/jquery-3.2.1.slim.min.js', [], false, true);
    wp_register_script('AOS', 'https://unpkg.com/aos@2.3.1/dist/aos.js', [], false, true);
    wp_enqueue_style('bootstrap');
    wp_enqueue_style('AOS');
    wp_enqueue_script('bootstrap');
    wp_enqueue_script('AOS');
    wp_enqueue_script('js', get_template_directory_uri() . '/script.js', [], false, true);
    wp_enqueue_style('stylecss', get_stylesheet_uri()); // Récupère l'URI de la feuille de style pour le thème actuel.
    wp_enqueue_style('stylecss-page', get_template_directory_uri() . '/style-page.css'); // Récupère l'URI de la deuxième feuille de style pour le thème actuel.

}

function montheme_title_separator() // Cette fonction permet de rajouter un séparateur dans le nom de la page sur l'onglet de navigation
{
    return '|';
}

function register_navwalker()
{
    require_once  get_template_directory() . '/class-wp-bootstrap-navwalker.php';
}

function montheme_pagination() // Cette fonction permet d'utiliser la pagination de boostrap
{
    $pages = paginate_links(['type' => 'array']); // On récupère les liens paginés pour les pages de publication d'archives.
    if ($pages === null) { // Si tous les articles sont présents sur la page => la pagination est nulle => on retourne juste le résultat
        return;
    }
    echo '<nav aria-label="Pagination" class="my-4">'; // On ajoute les balises <nav> de boostrap désirées
    echo '<ul class="pagination">'; // On ajoute les balises <ul> de boostrap désirées
    foreach ($pages as $page) {
        $active = strpos($page, 'current') !== false; // Lorsqu'une pagination est active, WP donne une classe à cette élément "current". Nous nous souhaitons une classe "active" avec boostrap
        $class = 'page-item';
        if ($active) { // Si la classe est "current" nous lui reatribuons la classe "page-item active"
            $class .= ' active';
        }
        echo '<li class="' . $class . '">'; // Nous affichons pour chaque élément du tableau les li avec leur classe ("page-item" ou "page-item active")
        echo str_replace('page-numbers', 'page-link', $page); // Nous remplaçons la classe des <a> valant "page-numbers" en "page-link"
        echo '</li>';
    }
    echo '</ul>';
    echo '</nav>';
}

function montheme_register_post_types()
{
    // La déclaration de nos Custom Post Types et Taxonomies ira ici
}

add_action('after_setup_theme', 'montheme_supports'); // Hook est appelé lors de chaque chargement de page, après l'initialisation du thème
add_action('wp_enqueue_scripts', 'montheme_register_assets'); // Hook utilisé avec add_theme_support
add_action('after_setup_theme', 'register_navwalker'); // Hook utilisé pour le walker (fichier class-wp-boostrap.php)
add_action('init', 'montheme_register_post_types'); // Hook utilisé pour les CPT

add_filter('document_title_separator', 'montheme_title_separator'); // Filtre utilisé lors de la mise en file d'attente de scripts et de styles destinés à apparaître sur le front-end
