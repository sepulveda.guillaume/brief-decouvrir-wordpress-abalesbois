<?php get_header() ?>

<?php if (have_posts()) : ?>
    <div class="row">
        <?php while (have_posts()) : the_post(); ?>
            <!-- Si des articles existent -->
            <div class="col-sm-4 my-3">
                <div class="card">
                    <?php the_post_thumbnail('medium', ['class' => 'card-img-top', 'alt' => '', 'style' => 'height: auto; width: "100%";']) ?>
                    <!-- On affiche l'image au dimension défini par add_image_size, en lui assignant un style et une classe voulue -->
                    <div class="card-body">
                        <h5 class="card-title"><?php the_title(); ?></h5> <!-- On afiche le titre de cet article -->
                        <h6 class="card-subtitle mb-2 text-muted"><?php the_category(); ?></h6> <!-- On afiche la catégorie de cet article -->
                        <p class="card-text">
                            <?php the_excerpt(); ?>
                            <!-- On afiche l'extrait de cet article -->
                        </p>
                        <a href="<?php the_permalink(); ?>" class="card-link">Voir plus</a> <!-- On renvoie vers le lien de l'article lorsque l'utilisateur appuie sur "Voir plus" -->
                    </div>
                </div>
            </div>
        <?php endwhile; ?>
    </div>

    <?php montheme_pagination(); ?>
    <!-- On utilise la function de pagination définie dans functions.php -->

<?php else : ?>
    <p>Aucun article.</p> <!-- On afiche aucun article si aucun article n'est trouvé -->
<?php endif; ?>

<?php get_footer(); ?>