<?php get_header() ?>

</div>

<div class="page-contact">
    <div class="container">
        <?php while (have_posts()) : the_post(); ?>
            <h1><?php the_title() ?></h1>
            <div class="formulaire" data-aos="fade-up">
                <?php the_content() ?>
            </div>
            <!-- Affiche le contenu de la page -->
        <?php endwhile; ?>
    </div>
</div>

<?php get_footer() ?>