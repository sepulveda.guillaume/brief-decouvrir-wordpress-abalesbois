<?php get_header() ?>

</div>

<section class="engagements">
    <div class="container">

        <?php while (have_posts()) : the_post(); ?>
            <h1><?php the_title() ?></h1>
            <!-- Affiche le titre -->
        <?php endwhile; ?>

        <?php
        // 1. On définit les arguments pour définir ce que l'on souhaite récupérer
        $args = array(
            'post_type' => 'engagement',
            'order' => 'ASC',
        );

        // 2. On exécute la WP Query
        $my_query = new WP_Query($args);

        if ($my_query->have_posts()) : ?>
            <?php while ($my_query->have_posts()) : $my_query->the_post(); ?>
                <!-- Si des articles existent -->
                <div class="row pt-5">
                    <div class="col-6 text-center" data-aos="fade-right">
                        <?php the_post_thumbnail('medium', ['alt' => '', 'style' => 'height: auto; width: "100%";']) ?>
                    </div>
                    <div class="col-6 d-flex flex-column flex-wrap justify-content-center" data-aos="fade-left">
                        <!-- On affiche l'image au dimension défini par add_image_size, en lui assignant un style et une classe voulue -->
                        <h5 class="pots-title"><?php the_title(); ?></h5> <!-- On afiche le titre de cet article -->
                        <h6 class="post-subtitle mb-2 text-muted"><?php the_category(); ?></h6> <!-- On afiche la catégorie de cet article -->
                        <?php the_content(); ?>
                    </div>
                </div>
            <?php endwhile; ?>

            <?php montheme_pagination(); ?>
            <!-- On utilise la function de pagination définie dans functions.php -->

        <?php else : endif;

        // 4. On réinitialise à la requête principale (important)
        wp_reset_postdata(); ?>

</section>
<?php get_footer(); ?>