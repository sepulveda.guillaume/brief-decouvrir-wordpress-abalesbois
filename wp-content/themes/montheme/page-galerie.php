<?php get_header() ?>

</div>

<div class="page-galerie">
    <div class="container">
        <?php while (have_posts()) : the_post(); ?>
            <h1><?php the_title() ?></h1>
            <div class="row">
                <div class="col">
                    <?php the_content() ?>
                </div>
            </div>
            <!-- Affiche le contenu de la page -->
        <?php endwhile; ?>
    </div>
</div>

<?php get_footer() ?>