<?php get_header() ?>

</div>

<div class="page-template">
    <div class="container">
        <?php while (have_posts()) : the_post(); ?>
            <h1><?php the_title() ?></h1>
            <?php the_content() ?>
            <!-- Affiche le contenu de la page -->
        <?php endwhile; ?>
    </div>
</div>

<?php get_footer() ?>