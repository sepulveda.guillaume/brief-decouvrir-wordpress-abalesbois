// Ensemble des scripts utilisés pour modifier les classes définies par WordPress afin de faire des ancres ou d'appliquer les animations de AOS animate

menuPresentation = document.querySelectorAll(".menu-item-72 a");
menuPresentation.forEach((element) => {
  element.href =
    window.location.origin +
    "/brief-decouvrir-wordpress-abalesbois/#presentation";
});

menuIntervention = document.querySelectorAll(".menu-item-73 a");
menuIntervention.forEach((element) => {
  element.href =
    window.location.origin +
    "/brief-decouvrir-wordpress-abalesbois/#intervention";
});

menuPartenaires = document.querySelectorAll(".menu-item-71 a");
menuPartenaires.forEach((element) => {
  element.href =
    window.location.origin +
    "/brief-decouvrir-wordpress-abalesbois/#partenaires";
});

titleSelect = document.querySelector(".title");
if (titleSelect !== null) {
  titleSelect.setAttribute("data-aos", "fade-up");
}

subTitleSelect = document.querySelector(".sub-title");
if (subTitleSelect !== null) {
  subTitleSelect.setAttribute("data-aos", "fade-up");
}

blockSelect = document.querySelectorAll(".wp-block-media-text__content");
if (blockSelect.length !== 0) {
  blockSelect[0].setAttribute("data-aos", "fade-right");
  blockSelect[1].setAttribute("data-aos", "fade-left");
}

imageSelect = document.querySelectorAll(".wp-block-media-text__media");
if (imageSelect.length !== 0) {
  imageSelect[0].setAttribute("data-aos", "fade-left");
  imageSelect[1].setAttribute("data-aos", "fade-right");
}

cardSelect = document.querySelectorAll(".wp-image-278");
if (cardSelect.length !== 0) {
  cardSelect[0].classList.add("img-fluid");
  cardSelect[0].classList.add("rounded");
}

galerieSelect = document.querySelector(".page-galerie figure");
if (galerieSelect !== null) {
  galerieSelect.setAttribute("data-aos", "fade-up");
}

videoSelect = document.querySelector(".aiovg");
if (videoSelect !== null) {
  videoSelect.setAttribute("data-aos", "fade-up");
}

tracteurSelect = document.querySelectorAll(".wp-block-media-text");
if (tracteurSelect.length !== 0) {
  tracteurSelect[1].children[0].classList.add("img-fluid");
}
