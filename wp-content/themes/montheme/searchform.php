<form class="form-inline my-2 my-lg-0" action="<?= esc_url(home_url('/')) ?>">
    <!-- Récupère l'URL du site actuel en échappant les caractères non désirés -->
    <input class="form-control mr-sm-2" name="s" type="search" placeholder="Recherche" aria-label="Recherche" value="<?= get_search_query() ?>"> <!-- La chaîne de requête de recherche est transmise via esc_attr pour s'assurer qu'elle peut être placée en toute sécurité dans un attribut HTML -->
    <button class="btn btn-outline my-2 my-sm-0" type="submit">Rechercher</button>
</form>