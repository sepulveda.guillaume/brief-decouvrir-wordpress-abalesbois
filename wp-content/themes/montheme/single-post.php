<?php get_header() ?>

</div>

<?php if (have_posts()) : ?>

    <div class="article-template">
        <div class="container mb-5">
            <?php while (have_posts()) : the_post(); ?>
                <h2><?php the_title() ?></h2>
                <div class="row">
                    <div class="col-6 text-center">
                        <?php the_post_thumbnail('medium', ['alt' => '', 'style' => 'height: auto; width: "100%";']) ?>
                    </div>
                    <div class="col-6">
                        <?php the_content() ?>
                    </div>
                </div>
        <?php endwhile;
        endif; ?>

        </div>
    </div>

    <?php get_footer() ?>